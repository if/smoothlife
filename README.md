# Smoothlife

_Smoothlife_ is a generalization of Conway's game of life to a
continuous domain (in time as well as space).

If you're interested in how it works, here are some resources that
were helpful to me:
- [an implementation in python](https://github.com/duckythescientist/SmoothLife)
- [an intimidating blog post](https://0fps.net/2012/11/19/conways-game-of-life-for-curved-surfaces-part-1/)
  - the scary part is just a weirdly difficult discretization method
- [the original paper](https://arxiv.org/abs/1111.1567)


To run this program, you'll need FFTW and SDL 2.0.  Once you have the
dependencies, just `make && ./ca`.

If you want to play around with different configurations, you should
mess with the `#define`s at the top of `main.c`.
