# Quick reminder about Makefiles:
# $< = 1st prereq listed
# $^ = all prereqs
# $@ = name of target
# Rules are in form of:
# ---------------------
# target: prereqs
# \t	command

CC = gcc
CFLAGS = -Wall -g -O2
LIBS = -lSDL2 -lm -lfftw3
SOURCES = *.c
OBJECTS = $(SOURCES:.c=.o)
EXEC = ca

$(EXEC): $(SOURCES)
	$(CC) $(CFLAGS) $(LIBS) $^ -o $@

clean:
	rm -f $(EXEC) *.o
