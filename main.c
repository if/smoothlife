/** An attempt at smoothlife. */
#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <complex.h>
#include <fftw3.h>


#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

#define MOD(a,b) ((a)<0?((a)+(b)):((a)%(b)))


#define COLS 512
#define ROWS COLS
#define SCALE 1
#define SCREEN_WIDTH (COLS * SCALE)
#define SCREEN_HEIGHT (ROWS * SCALE)
#define ROLL

#define ALPHA_N 0.028
#define ALPHA_M 0.147

/* the canonical one */
/* #define BIRTH_1 0.257 */
/* #define BIRTH_2 0.336 */
/* #define DEATH_1 0.365 */
/* #define DEATH_2 0.549 */

/* ebullient */
#define BIRTH_1 0.269
#define BIRTH_2 0.34
#define DEATH_1 0.523
#define DEATH_2 0.746

/* worms */
/* #define BIRTH_1 0.212 */
/* #define BIRTH_2 0.244 */
/* #define DEATH_1 0.222 */
/* #define DEATH_2 0.397 */


/* gliders everywhere! */
/* #define BIRTH_1 0.269 */
/* #define BIRTH_2 0.289 */
/* #define DEATH_1 0.228 */
/* #define DEATH_2 0.406 */

/* fun at speed 0.8 */
/* #define BIRTH_1 0.24 */
/* #define BIRTH_2 0.25 */
/* #define DEATH_1 0.00 */
/* #define DEATH_2 0.4 */


#define RADIUS 4.0
#define OUTER_RADIUS (3.0 * RADIUS)
#define BOUNDARY 1.0
#define SPEED 0.2

#define PI 3.14
#define INNER_AREA (PI * RADIUS * RADIUS)
#define OUTER_AREA ((PI * OUTER_RADIUS * OUTER_RADIUS) - INNER_AREA)



double
logistic(double x, double a, double alpha)
{
        return 1.0 / (1 + exp((-4/alpha) * (x - a)));
}


double
threshold(double x, double a, double b)
{
        return logistic(x, a, ALPHA_N) * (1 - logistic(x, b, ALPHA_N));
}

double
interp(double x, double y, double m)
{
        return (x * (1 - logistic(m, 0.5, ALPHA_M))) +
                (y * logistic(m, 0.5, ALPHA_M));

        /* NOTE: linear interpolation leads to structures that
           "boil over" and end up way too stable */
        /* return (1.0 - m) * x  + m * y; */
}



void
render_cell(SDL_Renderer *ren, double cell, int x, int y)
{
        int c = 255 * cell;
        int x_scale = SCREEN_WIDTH/COLS;
        int y_scale = SCREEN_HEIGHT/ROWS;
        SDL_Rect rect = (SDL_Rect){.x=x*x_scale, .y=y*y_scale,
                                   .w=x_scale, .h=y_scale};

        
        /* highlight sickly cells (usually on edge of structures) */
        if (cell < 0.5 && cell > 0.3)
                SDL_SetRenderDrawColor(ren, 100, 100, 255, 255);
        /* highlight cells with invalid life vals */
        else if (cell < 0) SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
        else if (cell > 1) SDL_SetRenderDrawColor(ren, 0, 0, 255, 255);
        else SDL_SetRenderDrawColor(ren, c, c, c, 255);

        
        /* if (cell < 0) SDL_SetRenderDrawColor(ren, 255, 0, 0, 255); */
        /* else if (cell > 1) SDL_SetRenderDrawColor(ren, 0, 0, 255, 255); */
        /* else SDL_SetRenderDrawColor(ren, c, c, c, 255); */

        
        SDL_RenderFillRect(ren, &rect);
}




fftw_complex
*forward_fft(int w, int h, double data[w][h], unsigned mode)
{
        double *in;
        double complex *out;
        fftw_plan p;        

        in  = fftw_malloc(sizeof (*in) * w * h);
        out = fftw_malloc(sizeof (*out) * w * (1 + h/2));
        p = fftw_plan_dft_r2c_2d(w, h, in, out, mode);

        for (int n = 0; n < w * h; n++) {
                int i = n % w, j = n / w;
                in[n] = data[i][j];
        }


        fftw_execute(p);

        fftw_destroy_plan(p);
        fftw_free(in);

        
        return out;
}

double
*backward_fft(int w, int h, double complex *in)
{
        double *out;
        fftw_plan p;
        
        out = fftw_malloc(sizeof (*out) * w * h);
        p = fftw_plan_dft_c2r_2d(w, h, in, out, FFTW_ESTIMATE);

        fftw_execute(p);
        
        fftw_destroy_plan(p);
        fftw_free(in);

        /* results need to be scaled down by array size */
        int size = w * h;
        for (int i = 0; i < size; i++) {
                out[i] /= size;
        }
        
        return out;
}

/* Make a disc-shaped kernel with simple anti-aliasing. 
   Returns the sum of entries in the kernel. */
double
disc_kernel(int radius, double kernel[COLS][ROWS])
{
        double sum = 0.0f;
        
        for (int i = 0; i < COLS; i++) {
                for (int j = 0; j < ROWS; j++) {
                        /* tip: try changing the kernel shape by using
                           another distance function, or taking distance
                           relative to some other point */
                        double di = i - (COLS/2), dj = j - (ROWS/2);
                        /* double d = sqrt((di * di) + (dj * dj)); */
                        double d = hypot(di, dj);
                        double n;

                        if      (d > radius + (BOUNDARY/2)) n = 0.0f;
                        else if (d < radius - (BOUNDARY/2)) n = 1.0f;
                        else n = (radius + (BOUNDARY/2) - d) / BOUNDARY;
                        
                        sum += n;
                        
                        /* "roll" indices to extreme edges */
                        int x = i;
                        int y = j;
#ifdef ROLL
                        x = (i + (COLS/2)) % COLS;
                        y = (j + (ROWS/2)) % ROWS;
#endif
                        
                        kernel[x][y] = n;
                }
        }

        return sum;
}



/* NOTE: `ker' is expected to have dimensions (COLS) x (1 + ROWS/2) 
   and `field' should have dimensions COLS x ROWS.  The result will
   then have the same dimensions as `field'. */
double
*fft_convolve(double complex *ker, double complex *field)
{
        /* multiply corresponding elements of "kernel" and field */
        int n = COLS * (1 + ROWS/2);
        double complex *products = fftw_malloc(sizeof (*products) * n);
        
        for (int i = 0; i < n; i++) {
                products[i] = ker[i] * field[i];
        }
        
        /* perform inverse fft on the products */
        double *result = backward_fft(COLS, ROWS, products);
        
        return result;
}

double
do_rule(double state, double neighbs)
{
        return threshold(neighbs, interp(BIRTH_1, DEATH_1, state),
                         interp(BIRTH_2, DEATH_2, state));
}


/* Update a cell field in a discrete time-step. */
void
do_step(double inner_area, double outer_area,
        double complex *fft_inner, double complex *fft_outer,
        double cell_field[COLS][ROWS], double next_field[COLS][ROWS])
{
        double complex *fft_field =
                forward_fft(COLS, ROWS, cell_field, FFTW_ESTIMATE);
        double *states = fft_convolve(fft_field, fft_inner);
        double *neighbs = fft_convolve(fft_field, fft_outer);

        for (int n = 0; n < COLS * ROWS; n++) {
                /* normalize state and neighb values */
                double st = states[n]  / INNER_AREA;
                double nb = neighbs[n] / OUTER_AREA;
                
                double s = do_rule(st, nb);
                int i = n % COLS;
                int j = n / COLS;
                
                next_field[i][j] = s;
        }

        fftw_free(fft_field);
        fftw_free(states);
        fftw_free(neighbs);
}


void
splotch(int x, int y, int size, double cells[COLS][ROWS])
{
        for (int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                        cells[(x+i)%COLS][(y+j)%ROWS] = 1.0f;
}


int
main()
{
        SDL_Window *win = NULL;
        SDL_Renderer *ren;
        SDL_Event event;
        double cells[COLS][ROWS];


        double ker_inner[COLS][ROWS];        
        double ker_outer[COLS][ROWS];
        double inner_area = disc_kernel(RADIUS, ker_inner);
        double outer_area = disc_kernel(OUTER_RADIUS, ker_outer);
        
        /* punch an inner-kernel-shaped hole in the outer kernel */
        outer_area -= inner_area;
        for (int i = 0; i < COLS; i++) {
                for (int j = 0; j < ROWS; j++) {
                        ker_outer[i][j] -= ker_inner[i][j];
                }
        }


        double complex *fft_inner = forward_fft(COLS, ROWS, ker_inner, FFTW_MEASURE);
        double complex *fft_outer = forward_fft(COLS, ROWS, ker_outer, FFTW_MEASURE);
        
        if (SDL_Init(SDL_INIT_VIDEO) < 0) {
                printf("SDL had an error: %s\n", SDL_GetError());
                return 0;
        }
        
        SDL_CreateWindowAndRenderer(SCREEN_WIDTH, SCREEN_HEIGHT, 0, &win, &ren);
        
        /* for (int i = 0; i < COLS; i++) { */
        /*         for (int j = 0; j < ROWS; j++) { */
        /*                 double f = ker_inner[i][j]; */
        /*                 int    c = f * 255; */
        /*                 SDL_SetRenderDrawColor(ren, c, c, c, 255); */
        /*                 SDL_RenderDrawPoint(ren, i, j); */
        /*         } */
        /* } */

        /* SDL_RenderPresent(ren); */
        
        /* while (1) { */
        /*         /\* exit main loop when window is closed *\/ */
        /*         if (SDL_PollEvent(&event) && event.type == SDL_QUIT) break; */
        /*         /\* SDL_RenderPresent(ren); *\/ */
        /* } */
        
        /* return 0; */

        /* init cells */
        for (int x = 0; x < COLS; x++)
                for (int y = 0; y < ROWS; y++)
                        cells[x][y] = 0.0f;
        
        srand(time(NULL));
        for (int i = 0; i < COLS; i++) {
                for (int j = 0; j < ROWS; j++) {
                        if ((rand() % 1000) <= 10)
                                splotch(i, j, 5, cells);
                }
        }

        
        puts("entering main loop!");
        while (1) {
                /* exit main loop when window is closed */
                if (SDL_PollEvent(&event) && event.type == SDL_QUIT) break;

                /* draw and update all of the cells */
                double new_cells[COLS][ROWS];
                do_step(inner_area, outer_area, fft_inner, fft_outer, cells, new_cells);
                
                for (int x = 0; x < COLS; x++) {
                        for (int y = 0; y < ROWS; y++) {
                                double cell = cells[x][y];
                                double   dc = new_cells[x][y];
                                
                                render_cell(ren, cell, x, y);

                                new_cells[x][y] = cell + ((dc - cell) * SPEED);
                        }
                }
                
                SDL_RenderPresent(ren);

                memcpy(cells, new_cells, sizeof(cells));
        }


        fftw_free(fft_inner);
        fftw_free(fft_outer);
        
        SDL_DestroyRenderer(ren);
        SDL_DestroyWindow(win);
        SDL_Quit();
        return 0;
}
